package Client;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by admin on 18.12.2017.
 */
public class Client {
    private static Socket socket;

    public static void main(String[] args) {
        connect();
        dataAuth();
        Thread threadReader = new Thread(new ReaderMessage(socket));
        threadReader.start();
        handler();
//        close();
    }

    private static void connect() {
        try {
            socket = new Socket("127.0.0.1", 4999);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void close() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void handler() {
        try {
            DataOutputStream os = new DataOutputStream(socket.getOutputStream());
            String message = enterMessage();
            while (!message.equals("exit")) {
                os.writeUTF(message);
                os.flush();
                message = enterMessage();
            }
            close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String enterMessage() {
        System.out.println("Enter message:");
        String message = null;
        Scanner scanner = new Scanner(System.in);
        message = scanner.nextLine();
        return message;
    }

    private static void dataAuth() {
        try {
            DataOutputStream os = new DataOutputStream(socket.getOutputStream());
            String nickname = null;
            String password = null;
            System.out.println("Enter your nickname :");

            Scanner scanner = new Scanner(System.in);
            nickname = scanner.nextLine();
//           5
            AuthClass authData = new AuthClass(nickname);
            authData.write(os);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
