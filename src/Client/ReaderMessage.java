package Client;

import Server.ClientHandler;

import java.io.*;
import java.net.Socket;

/**
 * Created by admin on 18.12.2017.
 */
public class ReaderMessage implements Runnable {
    private Socket socket;

    public ReaderMessage(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        BufferedReader bufferedReader;
        while (true) {
            try {
                DataInputStream is = new DataInputStream(socket.getInputStream());
                if (is.available() > 0) {
                    String message = is.readUTF();
                    System.out.println(message);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}