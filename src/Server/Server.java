package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 18.12.2017.
 */
public class Server {
    private static ServerSocket serverSocket;
    static List<Socket> listSocket = new ArrayList<>();
    public static void main(String[] args) {

        startSever();
        handle();
        closeServer();
    }

    private static void handle(){
        System.out.println("Server is listens...");
        while(true){
            try {
                Socket socketClient = serverSocket.accept();
                listSocket.add(socketClient);
                new Thread (new ClientHandler(socketClient)).start();
                Thread.sleep(10);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    private static void startSever(){
        try {
            serverSocket = new ServerSocket(4999);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void closeServer(){
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
