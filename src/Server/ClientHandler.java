package Server;

import Client.AuthClass;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 18.12.2017.
 */
public class ClientHandler implements Runnable {
    private final Socket socketClient;
    static private Map<String, AuthClass> map = new HashMap<>();
    boolean isFirst = true;
    AuthClass auth;

    public static void main(String[] args) {
    }

    public ClientHandler(Socket socketClient) {
        this.socketClient = socketClient;
    }

    @Override
    public void run() {
        String nickname = "";
        String message = null;
        while (true) {
            try {
                DataInputStream is = new DataInputStream(socketClient.getInputStream());

                if (is.available() > 0) {
                    if (isFirst) {
                         nickname = is.readUTF();
                        message = "Я подключился к чату";
                        isFirst = false;
                    } else message = is.readUTF();
                    System.out.println(message);
                    for (Socket socket : Server.listSocket) {
                        if (socket.isConnected()) {
                            DataOutputStream os = new DataOutputStream(socket.getOutputStream());
                            os.writeUTF(nickname + ": " + message);
                            os.flush();
                        } else Server.listSocket.remove(socket);
                    }
                }
                if (socketClient.isClosed()) {
                    Server.closeServer();
                }
                //     Thread.sleep(10);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}